<?php

if( ! defined('NANO_VERSION') || NANO_VERSION < 2 ) {
    throw new Exception('Nano Framework version 2.x is required.');
}

use Nano\Component\Loader as Loader;
use Pagebuilder\Model\Page as Page;

class PlgSearchPagebuilder extends JPlugin
{

	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	public function onContentSearchAreas()
	{
		static $areas = [
			'pagebuilder' => 'plg_search_pagebuilder_pages'
		];

		return $areas;
	}

	public function onContentSearch($text, $phrase = '', $ordering = '', $areas = null)
	{
		(new Loader)->addPrefix('Pagebuilder', JPATH_ADMINISTRATOR . '/components/com_pagebuilder')->register();

		$results = [];

		if ( $text === '' ) {
			return $results;
		}
		
		$pages = Page::where('state', 1)->get()->filter(function($page) use ($text) {
			return preg_match('/' . preg_quote($text, '/') . '/i', $page->title) || preg_match('/' . preg_quote($text, '/') . '/i', json_encode($page->body));
		});

		function simplify($key, $item, $results) {

			if ( is_string($item) && in_array($key, ['title', 'text', 'link_text']) ) {
				$results[] = strip_tags($item);
			}

			if ( is_array($item) || is_object($item) ) {
				foreach ( $item as $key => $child ) {
					$results = simplify($key, $child, $results);
				}
			}

			return $results;
		}

		foreach ( $pages as $page ) {

			$body = simplify('', $page->body, []);

			$matches = collect($body)->filter(function($item) use ($text) {
				return preg_match('/' . preg_quote($text, '/') . '/i', $item);
			});

			$results[] = toObject([
				'title' 	 => $page->title,
				'text' 		 => implode(' ... ', count($matches) ? $matches->toArray() : $body),
				'created' 	 => $page->full_created_at,
				'href' 	 	 => $page->showLink,
				'section'  	 => trans('plg_search_pagebuilder_page'),
				'browsernav' => ''
			]);
		}

		return $results;
	}

}
